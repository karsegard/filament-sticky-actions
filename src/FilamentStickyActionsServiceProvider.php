<?php

namespace KDA\FilamentStickyActions;

use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use KDA\FilamentStickyActions\Commands\FilamentStickyActionsCommand;
use Filament\PluginServiceProvider;
class FilamentStickyActionsServiceProvider extends PluginServiceProvider
{
    protected array $styles = [
        //    'my-package-styles' => __DIR__ . '/../dist/app.css',
            'filament-sticky-actions' => __DIR__ . '/../assets/css/filament-sticky-actions.css',
    
        ];

    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('filament-sticky-actions') ;
    }
}
